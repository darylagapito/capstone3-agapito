import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'

const Footer = () => {
    return (
        <footer>
            <Container fluid>
                <Row className="py-5 ms-5 pb-3 text-start">
                 
                    <Col sm={4} className="d-flex flex-row justify-content-start">
                    <h4 className="me-2"><i class="fab fa-facebook-f"></i></h4>
                    <h4 className="mx-2"><i class="fab fa-instagram"></i></h4>
                    <h4 className="mx-2"><i class="fab fa-twitter"></i></h4>
                    
                    
                    </Col>
                    
                    <Col sm={4} className='text-center py-3'>
                    
                    </Col>

                    <Col sm={4}>
                    
                    </Col>
                </Row>
                <Row >
                    <Col>
                        <p className="mb-3 ms-5">&copy; 2021 Street Altitude</p>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default Footer
