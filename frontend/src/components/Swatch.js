import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, Image, ListGroup, Card, Button, Form, ButtonToolbar, ButtonGroup } from 'react-bootstrap'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { listProductDetails } from '../actions/productActions'


const [size, setSize] = useState(0)
    
    
    <ButtonToolbar 
        className="justify-content-between">
        <ButtonGroup className="d-flex flex-wrap" aria-label="First group"  value={size} onChange={(e) => 
            setSize(e.target.value)}>
        
        {
            product.sizeInStock &&
            product.sizeInStock.map(x => (

                Object.values(x) > 0 ?
                
                <Button className="size-swatch" variant="light" key={x} value={Object.values(x)}>{Object.keys(x)}</Button>  
                : null
                
            ))
        }    
        </ButtonGroup>
    </ButtonToolbar>

export default Swatch