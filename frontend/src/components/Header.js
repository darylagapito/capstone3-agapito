import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Container, Nav, Navbar, Row, Col, Image } from 'react-bootstrap'
import { logout } from '../actions/userActions'

const Header = () => {
 
    const dispatch = useDispatch()
    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const logoutHandler = () => {
        dispatch(logout())
        window.location.replace('/login')
    }

    
    
    return (
        <header>
            <Container fluid>
                <Row id="top-header" className="text-center">
                    <Col>
                        <p className="my-1">Enjoy FREE Standard Shipping on orders over &#8369;10,000</p> 
                    </Col>
                </Row>
            </Container>

            <Navbar className="border-bottom navbar" variant="light" collapseOnSelect>
            <Container fluid>
                <LinkContainer to='/'>
                    <Navbar.Brand><Image className="logo" src="/images/logo.png" /></Navbar.Brand>
                </LinkContainer>
                
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto mx-3">

                {userInfo && userInfo.isAdmin ? null : 
                <LinkContainer to='/cart'>
                    <Nav.Link><i className="fas fa-shopping-cart mx-1"></i></Nav.Link>
                </LinkContainer>
                }
                {userInfo ? userInfo.isAdmin ?
                <>  
                        
                 
                        <LinkContainer to='/admin/cms'>
                        <Nav.Link><i class="fas fa-user-cog mx-1"></i></Nav.Link>
                        </LinkContainer>
                    
                        <Nav.Item>
                        <Nav.Link onClick={logoutHandler}>
                            <i className="fas fa-sign-out-alt mx-1"></i>
                        </Nav.Link>
                        </Nav.Item>

                </> : <>


                    <LinkContainer to='/profile'>
                        <Nav.Link><i className="fas fa-user mx-1"></i></Nav.Link>
                    </LinkContainer>
               
                    <Nav.Item>
                        <Nav.Link onClick={logoutHandler}>
                            <i className="fas fa-sign-out-alt mx-1"></i>
                        </Nav.Link>
                    </Nav.Item>

                     
                </> 

                : (
                <LinkContainer to='/login'>
                    <Nav.Link><i className="fas fa-user mx-1"></i></Nav.Link>
                </LinkContainer>
                )}
                
                  
                </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>
        </header>
    )
}

export default Header
