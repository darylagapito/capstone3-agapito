import React from 'react'
import { Nav } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const CheckoutProcess = ({ login, shipping, payment, order }) => {
    return (
       <Nav className="justify-content-center">
           <Nav.Item>
               {login ? (
                   <LinkContainer to='/login'>
                       <Nav.Link>Log in</Nav.Link>
                   </LinkContainer>
               ) : (<Nav.Link disabled>Log in</Nav.Link>)}
           </Nav.Item>

           <Nav.Item>
               {shipping ? (
                   <LinkContainer to='/checkout'>
                       <Nav.Link>Shipping</Nav.Link>
                   </LinkContainer>
               ) : (<Nav.Link disabled >Shipping</Nav.Link>)}
           </Nav.Item>

           <Nav.Item>
               {payment ? (
                   <LinkContainer to='/payment'>
                       <Nav.Link>Payment</Nav.Link>
                   </LinkContainer>
               ) : (<Nav.Link disabled style={{color:"white"}}>Payment</Nav.Link>)}
           </Nav.Item>

           <Nav.Item>
               {order ? (
                   <LinkContainer to='/order'>
                       <Nav.Link>Order</Nav.Link>
                   </LinkContainer>
               ) : (<Nav.Link disabled>Order</Nav.Link>)}
           </Nav.Item>
       </Nav>
    )
}

export default CheckoutProcess
