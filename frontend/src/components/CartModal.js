import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, ListGroup, Image, Form, Button, Container } from 'react-bootstrap'
import { addToCart, removeFromCart } from '../actions/cartActions'

const CartModal = ({ match, location, history }) => {
    const productId = match.params.id
    const size = location.search ? location.search.split(/[=?]/)[2]  : null
    const qty = location.search ? location.search.split('=')[2] : null
    const dispatch = useDispatch()

    const cart = useSelector(state => state.cart)
    const { cartItems } = cart

    useEffect(() => {
        if(productId) {
            dispatch(addToCart(productId, size, qty))
           
        }
    }, [dispatch, productId, size, qty])

    const removeFromCartHandler = (id) => {
       dispatch(removeFromCart(id))
    }
   
    const checkoutHandler = () => {
        history.push('/login?redirect=checkout')
    }
    
    return (
    
    <Container>
        <Row className="justify-content-center">
            <Col xs={12} sm={8}>
                
                {cartItems.length === 0 
                ? <>
                    <Row className="justify-content-center my-5">
                        <Col xs={12} md={5}>
                        <p className="text-center my-3"> Shopping Cart is empty.</p>.
                        <Link className='btn btn-dark p-3 col-12' to="/">START SHOPPING</Link>
                        </Col>
                    </Row>
                    </>
                : (
                    
                    <ListGroup variant='flush'>
                        <h4 className="my-5 text-center">Shopping Cart</h4>
                        {cartItems.map(item => (
                            <ListGroup.Item key={item.product} className="my-3">
                                <Row>
                                    <Col md={3}>
                                        <Image src={item.image} alt={item.name} fluid />
                                    </Col>

                                    
                                    <Col md={6}>
                                        <Row className="mb-1">
                                        <Link to={`/product/${item.product}`}>{item.name}</Link>
                                        </Row>

                                        <Row className="mb-1">
                                            <Col>
                                                US Size: {item.size}
                                            </Col>
                                        </Row>

                                        <Row className="mb-1">
                                            <Col>
                                                {item.price.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}
                                            </Col>
                                        </Row>
                                    </Col>

                                    <Col xs={6} md={2} className="mb-3">
                                        <Col> 
                                           <Form.Control
                                            as='select'
                                            className="form-select"
                                            value={item.qty}
                                            onChange={(e) => dispatch(addToCart(item.product, item.size,
                                            Number(e.target.value)))}
                                        >
                                    
                                        {
                                           item.sizeInStock?.map((x, index) => (
                                            Object.keys(x) == item.size ?
                                            
                                            [...Array(Number(Object.values(x))).keys()].map((n) => (
                                            <option key={n + 1} value={n + 1}>
                                            {n + 1}
                                            </option>
                                            ))
                                            : null
                                            )) 
                                        }    

                                        </Form.Control>

                                    </Col>
                                    </Col>

                                    <Col className="text-center" xs={6} md={1}>
                                        <Button className="border-0" type='button' variant='light' onClick={()=> removeFromCartHandler(item.product)}>
                                            <i className='fas fa-trash'></i>
                                        </Button>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                        ))}

                    {/* subtotal and checkout */}
                    <Row className="justify-content-center my-3">
                        <Col>
                        <p className="text-end"><span className="mx-2">Subtotal:</span> {cartItems.reduce((acc, item) => acc + Number(item.qty) * item.price, 0).toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}</p>
                        <p className="text-end"><span className="mx-2">Qty:</span>{(cartItems.reduce((acc, item) => acc + Number(item.qty), 0))} {(cartItems.reduce((acc, item) => acc + Number(item.qty), 0)) > 1 ? 'Items' : 'Item'} </p>  
                        <p className="text-center">Shipping and Handling Fees are calculated at checkout.</p>
                        <Button 
                            className='btn-dark col-12 p-3 '
                            type='button'
                            onClick={checkoutHandler}
                            >PROCEED TO CHECKOUT
                           </Button>
                        </Col>
                    </Row>
                    </ListGroup>

                    
                                        
                  

                )}
            </Col>
        </Row>
    </Container>
    )
}

export default CartModal
