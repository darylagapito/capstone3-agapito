import React from 'react'
import { Link } from 'react-router-dom'
import { Card } from 'react-bootstrap'

const Product = ({ product }) => {
    return (
        <Card className='mt-3 mx-3 p-3 border-0 product-card h-100'>
            <Link to={`/product/${product._id}`}>
                <Card.Img src={product.image} variant='top'></Card.Img>
                
                <Card.Title as='div' className="text-center mt-1 m-0 p-0">
                   <strong>{product.name}</strong>
                </Card.Title>
            </Link>

            <Card.Body className="d-flex text-center align-items-end justify-content-center p-0">
                <Card.Text as='div'>
                    <h6>{product.brand}</h6>
                    <p>{ product.price.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'}) }</p>
                </Card.Text>
            </Card.Body>
        </Card>
    )
}

export default Product
