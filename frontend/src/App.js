import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Container } from 'react-bootstrap'
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import LoginScreen from "./screens/LoginScreen"
import RegisterScreen from "./screens/RegisterScreen"
import ProfileScreen from "./screens/ProfileScreen"
import Cart from "./components/CartModal";
import CheckoutScreen from "./screens/CheckoutScreen";
import PaymentScreen from "./screens/PaymentScreen";
import CompleteOrderScreen from "./screens/CompleteOrderScreen";
import OrderScreen from "./screens/OrderScreen";
import UserListScreen from "./screens/UserListScreen";
import UserEditScreen from "./screens/UserEditScreen";
import ProductListScreen from "./screens/ProductListScreen";
import ProductEditScreen from "./screens/ProductEditScreen";
import OrderListScreen from "./screens/OrderListScreen";
import ProductsAllScreen from "./screens/ProductsAllScreen";
import CMS from "./screens/CMS";

const App = () => {
  return (
   
    <Router>
      <Header />
        <main className="my-3">
          
            
            <Route path='/' component={HomeScreen} exact />

            <Container>
            <Switch>
            <Route path='/login' component={LoginScreen} exact />
            <Route path='/register' component={RegisterScreen} exact />
            <Route path='/profile' component={ProfileScreen} exact />
            <Route path='/checkout' component={CheckoutScreen} exact />
            <Route path='/payment' component={PaymentScreen} exact />
            <Route path='/orders' component={CompleteOrderScreen} exact />
            <Route path='/product/:id' component={ProductScreen} exact />
            <Route path='/cart/:id?' component={Cart} exact/>
            <Route path='/products' component={ProductsAllScreen} exact />
            <Route path='/orders/:id' component={OrderScreen} exact />
            <Route path='/admin/users' component={UserListScreen} exact />
            <Route path='/admin/products' component={ProductListScreen} exact />
            <Route path='/admin/orders' component={OrderListScreen} exact />
            <Route path='/admin/users/:id/edit' component={UserEditScreen} />
            <Route path='/admin/products/:id/edit' component={ProductEditScreen} />
            <Route path='/admin/cms' component={CMS} />
            
            </Switch>
          </Container>
        </main>
      <Footer />
    </Router>

  );
}

export default App;
