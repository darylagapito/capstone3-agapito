import axios from "axios";
import { ORDER_USER_LIST_RESET, ORDER_DETAILS_RESET } from "../constants/orderConstants";
import { 
    USER_DELETE_FAIL,
    USER_DELETE_REQUEST,
    USER_DELETE_SUCCESS,
    USER_LIST_FAIL,
    USER_LIST_REQUEST,
    USER_LIST_SUCCESS,
    USER_LOGIN_FAIL, 
    USER_LOGIN_REQUEST, 
    USER_LOGIN_SUCCESS, 
    USER_LOGOUT, 
    USER_PROFILE_FAIL, 
    USER_PROFILE_REQUEST, 
    USER_PROFILE_RESET, 
    USER_PROFILE_SUCCESS, 
    USER_REGISTER_FAIL, 
    USER_REGISTER_REQUEST,
    USER_REGISTER_SUCCESS,
    USER_UPDATE_ADMIN_FAIL,
    USER_UPDATE_ADMIN_REQUEST,
    USER_UPDATE_ADMIN_SUCCESS,
    USER_UPDATE_FAIL,
    USER_UPDATE_REQUEST,
    USER_UPDATE_SUCCESS
} from "../constants/userConstants";

export const login = (email, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_LOGIN_REQUEST
        })

        const headersConfig = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const { data } = await axios.post('/users/login', { email, password }, headersConfig)

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        })
        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: USER_LOGIN_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}

export const logout = () => (dispatch) => {
    localStorage.removeItem('userInfo')
    localStorage.removeItem('shippingAddress')
    dispatch({ type: USER_LOGOUT })
    dispatch({ type: USER_PROFILE_RESET })
    dispatch({ type: ORDER_USER_LIST_RESET })
    dispatch({ type: ORDER_DETAILS_RESET })
}


export const register = (fName, lName, mobileNo, email, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_REGISTER_REQUEST
        })

        const headersConfig = {
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const { data } = await axios.post('/users/', 
        { fName, lName, mobileNo, email, password }, headersConfig)

        dispatch({
            type: USER_REGISTER_SUCCESS,
            payload: data
        })
        
        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        })
        localStorage.setItem('userInfo', JSON.stringify(data))

    } catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const getUserProfile = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_PROFILE_REQUEST
        })
        const { userLogin: { userInfo } } = getState()
        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }
        
        const { data } = await axios.get(`/users/${id}`, headersConfig)
        
        dispatch({
            type: USER_PROFILE_SUCCESS,
            payload: data,
        })
        
    } catch (error) {
        dispatch({
            type: USER_PROFILE_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const updateUserProfile = (user) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_UPDATE_REQUEST
        })
        const { userLogin: { userInfo } } = getState()
        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }
        
        const { data } = await axios.put(`/users/profile`, user, headersConfig)
        
        dispatch({
            type: USER_UPDATE_SUCCESS,
            payload: data,
        })
        
    } catch (error) {
        dispatch({
            type: USER_UPDATE_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const listUsers = (user) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_LIST_REQUEST
        })
        const { userLogin: { userInfo } } = getState()
        const headersConfig = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`
            }
        }
        
        const { data } = await axios.get(`/users/`, headersConfig)
        
        dispatch({
            type: USER_LIST_SUCCESS,
            payload: data,
        })
        
    } catch (error) {
        dispatch({
            type: USER_LIST_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const deleteUser = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_DELETE_REQUEST
        })
        const { userLogin: { userInfo } } = getState()
        const headersConfig = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`
            }
        }
        
        await axios.delete(`/users/${id}`, headersConfig)
        
        dispatch({
            type: USER_DELETE_SUCCESS
        })
    } catch (error) {
        dispatch({
            type: USER_DELETE_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}

 
export const updateUserByAdmin = (user) => async (dispatch, getState) => {
    try {
        dispatch({
            type: USER_UPDATE_ADMIN_REQUEST
        })
        const { userLogin: { userInfo } } = getState()
        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }
        
        const { data } = await axios.put(`/users/${user._id}`, user, headersConfig)
        
        dispatch({
            type: USER_UPDATE_ADMIN_SUCCESS
        })

        dispatch({
            type: USER_PROFILE_SUCCESS,
            payload: data,
        })
    } catch (error) {
        dispatch({
            type: USER_UPDATE_ADMIN_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}