import axios from "axios";
import { ORDER_ADD_FAIL,
    ORDER_ADD_SUCCESS,
    ORDER_ADD_REQUEST,
    ORDER_DETAILS_SUCCESS,
    ORDER_DETAILS_FAIL,
    ORDER_DETAILS_REQUEST,
    ORDER_PAY_REQUEST,
    ORDER_PAY_SUCCESS,
    ORDER_PAY_FAIL,
    ORDER_USER_LIST_FAIL,
    ORDER_USER_LIST_SUCCESS,
    ORDER_USER_LIST_REQUEST,
    ORDER_LIST_REQUEST,
    ORDER_LIST_SUCCESS,
    ORDER_LIST_FAIL
} from "../constants/orderConstants";

export const addOrder = (order) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ORDER_ADD_REQUEST
        })
        
        const {
            userLogin: { userInfo },
        } = getState()

        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.post('/orders', order, headersConfig)

        dispatch({
            type: ORDER_ADD_SUCCESS,
            payload: data,
            
        })

        localStorage.removeItem('cartItems')
    } catch (error) {
        dispatch({
            type: ORDER_ADD_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const getOrderDetails = (id) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ORDER_DETAILS_REQUEST
        })
        
        const {
            userLogin: { userInfo },
        } = getState()

        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.get(`/orders/${id}`, headersConfig)

        dispatch({
            type: ORDER_DETAILS_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_DETAILS_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}

export const payOrder = (orderId, paymentResult) => async (dispatch, getState) => {
    try {
        dispatch({
            type: ORDER_PAY_REQUEST
        })
        
        const {
            userLogin: { userInfo },
        } = getState()

        const headersConfig = {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.put(`/orders/${orderId}/pay`, paymentResult, headersConfig)

        dispatch({
            type: ORDER_PAY_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_PAY_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}

export const userListOrders = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: ORDER_USER_LIST_REQUEST
        })
        
        const {
            userLogin: { userInfo },
        } = getState()

        const headersConfig = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.get(`/orders/userorders`, headersConfig)

        dispatch({
            type: ORDER_USER_LIST_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_USER_LIST_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}


export const listOrders = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: ORDER_LIST_REQUEST
        })
        
        const {
            userLogin: { userInfo },
        } = getState()

        const headersConfig = {
            headers: {
                Authorization: `Bearer ${userInfo.token}`
            }
        }

        const { data } = await axios.get(`/orders/`, headersConfig)

        dispatch({
            type: ORDER_LIST_SUCCESS,
            payload: data
        })

    } catch (error) {
        dispatch({
            type: ORDER_LIST_FAIL,
            payload: error.response && error.response.data.message 
            ? error.response.data.message : error.message
        })
    }
}