import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Form, Button, Col, Table, Image } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { listProductDetails, updateProduct } from '../actions/productActions'
import { PRODUCT_UPDATE_RESET } from '../constants/productConstants'



const ProductEditScreen = ({ match, history }) => {
    const productId = match.params.id
    
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [brand, setBrand] = useState('')
    const [category, setCategory] = useState('')
    const [price, setPrice] = useState(0)
    const [image, setImage] = useState('')
    const [sizeInStock, setSizeInStock] = useState([{}])
    const [isActive, setIsActive] = useState()


    const dispatch = useDispatch()
    const productDetails = useSelector(state => state.productDetails)
    const { loading, error, product } = productDetails

    const productUpdate = useSelector(state => state.productUpdate)
    const { loading: loadingUpdate, error:errorUpdate, success:successUpdate } = productUpdate
    
    useEffect(() => { 
        if(successUpdate) {
            dispatch({ type: PRODUCT_UPDATE_RESET })
            history.push('/admin/products')
        } else {
            if(!product.name || product._id !== productId) {
                dispatch(listProductDetails(productId))
            } else {
                setName(product.name)
                setDescription(product.description)
                setBrand(product.brand)
                setCategory(product.category)
                setPrice(product.price)
                setImage(product.image)
                setSizeInStock([...product.sizeInStock])
                setIsActive(product.isActive)
            }
        }

    }, [dispatch, history, product, productId, successUpdate])



const submitHandler = (e) => {
    e.preventDefault();   
    dispatch(updateProduct({
        _id: productId,
        name, 
        description, 
        brand, category, 
        price, 
        image, 
        sizeInStock,
        isActive
    }))
   
}
    
    return (
        <>
         <Link to={`/admin/cms`}><p className="mt-4 return"><span className="me-2">&#10094;</span> Back to Users</p></Link>
         
           <Row className="text-center mt-5 mb-3">
           <h4>Update Product Details</h4>
           <p className="mb-5 text-danger">For Administrator Use Only</p>
           </Row>
            {loadingUpdate && <Loader />}
            {errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
            {loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message> : (
            
                
                <Form onSubmit={ submitHandler }>

                <Row>
                    <Col sm={6}>
                    <Form.Group controlId='fName' className="mb-3">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Name" 
                    value={name}
                    onChange={(e) => setName(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='description' className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Last Name" 
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='brand' className="mb-3">
                    <Form.Label>Brand</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Brand" 
                    value={brand}
                    onChange={(e) => setBrand(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='category' className="mb-3">
                    <Form.Label>Category</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Category" 
                    value={category}
                    onChange={(e) => setCategory(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='price' className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                    type='number' 
                    placeholder="Price" 
                    value={price}
                    onChange={(e) => setPrice(e.target.value)}>
                    </Form.Control>
                </Form.Group>


                    </Col>

                 {/* images */}
                    <Col sm={6}>

                    <Form.Group controlId='image' className="mb-5">
                    <Form.Label>Image</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Enter main image url" 
                    value={image}
                    onChange={(e) => setImage(e.target.value)}>
                    </Form.Control>
                    </Form.Group>

                    <Row className="mt-5 text-center justify-content-center">
                        <Col >
                            <Image id="big" className="w-50 my-5" src={image} alt="No Image" fluid />
                        </Col>
                    </Row>

                    </Col>
                </Row>
               
              
            



               
               




                {/* rest */}
                <p>Sizes in Stock</p>
                <Table bordered hover responsive="xs" className="text-center">
                                <thead>
                                    <tr>
                                        {product.sizeInStock?.map((x, index) => (
                                            <th key={index}>{Object.keys(x)}</th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                   
                                        {product.sizeInStock?.map((x, index) => (
                                            <th key={index}>
                                            <Form.Group controlId='sizeInStock' className="mb-3">
                                                <Form.Control 
                                                type='text' 
                                                placeholder="Qty" 
                                                value={Object.values(x)}
                                                onChange={(e) => setSizeInStock(e.target.value)}>
                                                </Form.Control>
                                            </Form.Group>    
                                            </th>
                                        ))}
                                    </tr>
                                </tbody>

                                   
                            </Table>

                <Form.Group controlId='isActive' className="mb-3">
                    <Form.Check
                    type='checkbox' 
                    label="Active" 
                    checked={isActive}
                    onChange={(e) => setIsActive(e.target.checked)}>
                    </Form.Check>
                </Form.Group>

 
                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                    UPDATE
                </Button>
                </Form>



            )}

           
        
        </>
       
    )
}

export default ProductEditScreen

