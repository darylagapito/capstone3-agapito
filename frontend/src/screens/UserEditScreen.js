import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Form, Button } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { getUserProfile, updateUserByAdmin } from '../actions/userActions'
import Forms from '../components/Forms'
import { USER_UPDATE_ADMIN_RESET } from '../constants/userConstants'


const UserEditScreen = ({ match, history }) => {
    const userId = match.params.id

    const [fName, setFName] = useState('')
    const [lName, setLName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [email, setEmail] = useState('')
    const [isAdmin, setIsAdmin] = useState()

    const dispatch = useDispatch()
    const userProfile = useSelector(state => state.userProfile)
    const { loading, error, user } = userProfile

    const userUpdateByAdmin = useSelector(state => state.userUpdateByAdmin)
    const { loading:loadingUpdate, error:errorUpdate, success:successUpdate } = userUpdateByAdmin

    //PUT THIS IN ORDER SCREEN
    useEffect(() => { 
        if(successUpdate){
            dispatch({ type: USER_UPDATE_ADMIN_RESET })
            history.push('/admin/cms')
        } else {
            if(!user.fName || user._id !== userId) {
                dispatch(getUserProfile(userId))
            } else {
                setFName(user.fName)
                setLName(user.lName)
                setEmail(user.email)
                setMobileNo(user.mobileNo)
                setIsAdmin(user.isAdmin)
            }
        }


     
    }, [dispatch, userId, history, user, successUpdate])

const submitHandler = (e) => {
    e.preventDefault();   
    dispatch(updateUserByAdmin({_id: userId, 
        fName, 
        lName, 
        email, 
        mobileNo, 
        isAdmin 
    }))

}

    return (
        <>
         <Link to={`/admin/cms`}><p className="mt-4 return"><span className="me-2">&#10094;</span> Back to Users</p></Link>
         
           <Row className="text-center mt-5 mb-3">
           <h4>Update User Information</h4>
           <p className="mb-5 text-danger">For Administrator Use Only</p>
           </Row>
            {loadingUpdate && <Loader />}
            {errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
            {loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message> : (
            
            <Forms>
                <Form onSubmit={ submitHandler }>

                <Form.Group controlId='fName' className="mb-3">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="First Name" 
                    value={fName}
                    onChange={(e) => setFName(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='lName' className="mb-3">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Last Name" 
                    value={lName}
                    onChange={(e) => setLName(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='email' className="mb-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                    type='email' 
                    placeholder="Email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='mobileNo' className="mb-3">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control 
                    type='text' 
                    placeholder="Mobile Number" 
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='isAdmin' className="mb-3">
                    <Form.Check
                    type='checkbox' 
                    label="Admin" 
                    checked={isAdmin}
                    onChange={(e) => setIsAdmin(e.target.checked)}>
                    </Form.Check>
                </Form.Group>

                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                    UPDATE
                </Button>
                </Form>


                </Forms>

            )}

           
        
        </>
       
    )
}

export default UserEditScreen
