import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Row, Col, Button, Table, Accordion, Card } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { listOrders } from '../actions/orderActions'


const OrderListScreen = ({ history, location }) => {
    const dispatch = useDispatch()

    const orderList = useSelector(state => state.orderList)
    const { loading, error, orders } = orderList

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    console.log(userInfo)
    useEffect(() => {
        if(userInfo && userInfo.isAdmin) {
            dispatch(listOrders())
        } else {
            window.location.replace('/login')
        }
       
    }, [dispatch, history, userInfo])
    console.log(orders)
    return (    
        <>
        <Row className="justify-content-center">
        <Col md={9}>
        <Row className="text-center my-3">
        <Col>
        <h4 >Orders</h4>
        <p className="mb-5 text-danger">For Administrator Use Only</p>
        {
        loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message>
        : ( 
        
        <>
            <Accordion id="user-list-accordion" className="text-start" defaultActiveKey="0">
                {
              
                orders?.map(order => (
                    <Card id="order-card" className="my-1" key={order._id}>
                    
                        <Accordion.Toggle as={Card.Header}  eventKey={order._id}>
                            <Col>
                            Order ID: {order._id} 
                            </Col>
                            <Col>
                            Name: {order.user.fName} {order.user.lName}
                            </Col>
                        </Accordion.Toggle>
                        <Accordion.Collapse className="accordion-collapse" eventKey={order._id}>
                        <Card.Body>
                            <Table borderless hover responsive="xs" className="text-center">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>USER</th>
                                        <th>DATE</th>
                                        <th>TOTAL</th>
                                        <th>PAID</th>
                                        <th>DELIVERED</th>
                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                            <td>{order._id}</td>
                                            <td>{order.user.fName} {order.user.lName}</td>
                                            <td>{order.createdAt.substring(0, 10)}</td>
                                            <td>{order.totalPrice.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}</td>
                                            <td className="text-center">{order.isPaid ? (<i class="fas fa-check text-success"></i>) : (<i class="fas fa-times text-danger"></i>)}</td>
                                            <td className="text-center">{order.isDelivered ? (<i class="fas fa-check text-success"></i>) : (<i class="fas fa-times text-danger"></i>)}</td>
                                           
                                    </tr>
                                </tbody>
                            </Table>
                            <Row>   
                            <Col>
                            <LinkContainer to={`/orders/${order._id}`}>
                                <Button variant='light' className='border-0 btn-sm user-list-buttons'>
                                    <i className="fas fa-edit"></i>
                                </Button>
                            </LinkContainer>
                            </Col>
                            </Row>
                           
                            
    
                        </Card.Body>
                        </Accordion.Collapse>

                    </Card>
                 ))}
                </Accordion>
                                         
                </>  
               
            )}
        
        </Col>
    </Row>
    </Col>
    </Row>

    </>
    )
}

export default OrderListScreen
