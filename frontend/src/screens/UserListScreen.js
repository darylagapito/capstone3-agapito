import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Row, Col, Button, Table, Accordion, Card } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { deleteUser, listUsers } from '../actions/userActions'
import { Redirect } from 'react-router-dom'


const UserListScreen = ({ history, location }) => {
    const dispatch = useDispatch()

    const userList = useSelector(state => state.userList)
    const { loading, error, users } = userList

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const userDelete = useSelector(state => state.userDelete)
    const { success:deleted } = userDelete

    useEffect(() => {
        if(userInfo && userInfo.isAdmin) {
            dispatch(listUsers())
        } else {
            history.push('/login')
        }
       
    }, [dispatch, history, userInfo, deleted])

    const deleteHandler = (id) => {
        if (window.confirm('Are you sure you want to delete this user?')){
            dispatch(deleteUser(id))
            window.location.reload();
        }
    }

    return (
        <>
        <Row className="justify-content-center">
        <Col md={9}>
        <Row className="text-center my-3">
        <Col>
        <h4 >Users</h4>
        <p className="mb-5 text-danger">For Administrator Use Only</p>
        {
        loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message>
        : ( 
        
        <>
            <Accordion id="user-list-accordion" className="text-start" defaultActiveKey="0">
                {users.map(user => (
                    <Card id="user-card" className="my-1" key={user._id}>
                    
                        <Accordion.Toggle as={Card.Header}  eventKey={user._id}>
                            <Col>
                            User ID: {user._id} 
                            </Col>
                            <Col>
                            Name: {user.fName} {user.lName}
                            </Col>
                        </Accordion.Toggle>
                        <Accordion.Collapse className="accordion-collapse" eventKey={user._id}>
                        <Card.Body>
                            <Table borderless hover responsive="xs" className="text-center">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Admin</th>
                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                            <td>{user.fName} {user.lName}</td>
                                            <td>{user.email}</td>
                                            <td>{user.mobileNo}</td>
                                            <td>{user.isAdmin ? (<i className="fas fa-check text-success"></i>) : (<i className="fas fa-times text-danger"></i>)}</td>
                                           
                                    </tr>
                                </tbody>
                            </Table>
                            <Row>   
                            <Col>
                            <LinkContainer to={`/admin/users/${user._id}/edit`}>
                                <Button variant='light' className='border-0 btn-sm user-list-buttons'>
                                    <i className="fas fa-edit"></i>
                                </Button>
                            </LinkContainer>
                            </Col>

                            <Col className="text-end">
                            <Button variant='light' className='border-0 user-list-buttons btn-sm border-0' onClick={() => deleteHandler(user._id)}><i className="fas fa-trash text-danger"></i></Button> 
                            </Col>
                            </Row>
                           
                            
    
                        </Card.Body>
                        </Accordion.Collapse>

                    </Card>
                 ))}
                </Accordion>
                                         
                </>  
               
            )}
        
        </Col>
    </Row>
    </Col>
    </Row>

    </>
    )
}

export default UserListScreen
