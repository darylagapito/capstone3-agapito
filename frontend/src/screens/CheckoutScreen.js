import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Form, Button } from 'react-bootstrap'
import { getUserProfile } from '../actions/userActions'
import Forms from '../components/Forms'
import CheckoutProcess from '../components/CheckoutProcess'
import { saveShippingAddress } from '../actions/cartActions'



const CheckoutScreen = ({ history }) => {
    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    const [address, setAddress] = useState(shippingAddress.address)
    const [city, setCity] = useState(shippingAddress.city)
    const [postalCode, setPostalCode] = useState(shippingAddress.postalCode)
    const [country, setCountry] = useState(shippingAddress.country)

    const dispatch = useDispatch()
    const userProfile = useSelector(state => state.userProfile)
    const { user } = userProfile

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    useEffect(() => { 
        if(!userInfo) {
        history.push('/login')
    } else {
        if(!user.fName) {
            dispatch(getUserProfile('profile'))
        } 
    }
    }, [history, dispatch, userInfo, user])

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(saveShippingAddress({ address, city, postalCode, country }))
        history.push('/payment')
    }
    
    return (
        <>
        <CheckoutProcess login shipping />
        <Forms>
            
            <Row className="text-center mt-5 mb-3">
            <h4>Shipping Details</h4>
            <p>Please review your contact information and enter your complete shipping address.</p>
            </Row>
            <Row className="text-start mt-5 mb-3">
            <p>Contact Information:</p>
            <p>Name: {user.fName} {user.lName}</p>
            <p>Mobile Number: {user.mobileNo}</p>
            </Row>

            <Form onSubmit={submitHandler} className="mt-5">
                <Form.Label>Shipping Address:</Form.Label>
                <Form.Group controlId='address' className="mb-3">
                    <Form.Control 
                    required
                    type='text' 
                    placeholder="House Number, Building, Street Name, etc." 
                    value={address}
                    onChange={(e) => setAddress(e.target.value)}>
                    </Form.Control>
                </Form.Group>  

                <Form.Group controlId='city' className="mb-3">
                    <Form.Control 
                    required
                    type='text' 
                    placeholder="City / Municipality" 
                    value={city}
                    onChange={(e) => setCity(e.target.value)}>
                    </Form.Control>
                </Form.Group>  

                <Form.Group controlId='postalCode' className="mb-3">
                    <Form.Control 
                    required
                    type='text' 
                    placeholder="Postal  Code" 
                    value={postalCode}
                    onChange={(e) => setPostalCode(e.target.value)}>
                    </Form.Control>
                </Form.Group>  

                <Form.Group controlId='country' className="mb-3">
                    <Form.Control 
                    required
                    type='text' 
                    placeholder="Country" 
                    value={country}
                    onChange={(e) => setCountry(e.target.value)}>
                    </Form.Control>
                </Form.Group>  

                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                   CONTINUE TO PAYMENT
                </Button>
            </Form>
        </Forms>
      
    </>
    )
}

export default CheckoutScreen
