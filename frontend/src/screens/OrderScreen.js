import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { PayPalButton } from 'react-paypal-button-v2'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, ListGroup, Image, Card, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { getOrderDetails, payOrder } from '../actions/orderActions'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { ORDER_PAY_RESET } from '../constants/orderConstants'
 

const OrderScreen = ({ match }) => {
    const orderId = match.params.id
    
    const dispatch = useDispatch()
    const [sdkReady, setSdkReady] = useState(false)

const userLogin = useSelector(state => state.userLogin)
const { userInfo } = userLogin
  

const orderDetails = useSelector(state => state.orderDetails)
const { order, loading, error } = orderDetails

const orderPay = useSelector((state) => state.orderPay)
const { loading:loadingPay, success:successPay } = orderPay



useEffect(() => {

      const addPayPalScript = async () => {
        const { data: clientId } = await axios.get('/config/paypal')
        const script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = `https://www.paypal.com/sdk/js?client-id=${clientId}`
        script.async = true
        script.onload = () => {
            setSdkReady(true)
        }
        document.body.appendChild(script)
    }

  
    if(!order || order._id !==orderId ||  successPay){   
        dispatch({ type: ORDER_PAY_RESET })
        dispatch(getOrderDetails(orderId))
        
    } else if (!order.isPaid) {
        if(!window.paypal) {
            addPayPalScript()
        } else {
            setSdkReady(true)
        }
        
    }
   
    
}, [dispatch, orderId, successPay, order])

const successPaymentHandler = (paymentResult) => {
    dispatch(payOrder(orderId, paymentResult))
    window.location.reload();
}


    return (
        loading ? <Loader /> : error ? <Message variant="danger">{error}</Message>
        : order &&
        <>
        <Container>
        <Link to={userInfo.isAdmin ? `/admin/cms` : `/profile` }><p className="mt-4 return"><span className="me-2">&#10094;</span> Back to Account</p></Link>
            <Row className="text-center mt-5 mb-3">
            <h4>Order Details</h4>
            <p>Order: {(order._id)?.toUpperCase()}</p>
            
            </Row>
            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                    
                        <ListGroup.Item></ListGroup.Item>
                        <ListGroup.Item>
                        <Row className="my-3"> 
                        <Col>
                        <p className="no-margins">Order placed on {new Date(order.createdAt).toLocaleString()}</p>
                        </Col>      
                        </Row>
                        </ListGroup.Item>
                        
                        <ListGroup.Item>
                            <Row className="my-3">
                            
                                <Col>
                                <span className="me-2"><strong>Contact Information: </strong></span>
                                <p className="no-margins mt-1">{order.user.fName} {order.user.lName}</p>
                                <p className="no-margins">{order.user.mobileNo}</p>
                                <p className="no-margins">{order.user.email}</p>
                                </Col>
                            </Row>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row className="my-3">
                                <Col>
                                <span className="me-2"><strong>Ship to: </strong></span>
                                {order.shippingAddress.address},{' '} 
                                {order.shippingAddress.postalCode}{' '}
                                {order.shippingAddress.city},{' '} 
                                {order.shippingAddress.country}.
                                </Col>
                            </Row>
                            {order.isDelivered ? <Message variant="success">This order has been delivered.</Message>
                            : <Message variant="danger">This order is not yet delivered.</Message>}
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row className="my-3">
                                <Col>
                                <span className="me-2"><strong>Payment Method: </strong></span>
                                {order.paymentMethod}
                                </Col>
                            </Row>
                            {order.isPaid ? <Message variant="success">Order paid on {new Date(order.paidDate).toLocaleString()}</Message>
                            : <Message variant="danger">Payment is not yet fulfilled.</Message>}
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row className="my-3">
                                <span className="me-2"><strong>Items: </strong></span>
                                {order.orderItems.length === 0 ? <Message>You do not have any orders.</Message>
                                : (
                                   <ListGroup variant='flush'>
                                       {order.orderItems.map((item, index) => (
                                           <ListGroup.Item key={index} >
                                               <Row className="my-3">
                                                   <Col md={2}>
                                                       <Image src={item.image} alt={item.name} fluid />
                                                   </Col>

                                                   <Col>
                                                        <Link to={`/product/${item.product}`}>
                                                            <p className="my-1">
                                                            {item.name}
                                                            </p>
                                                        </Link>
                                                            <p className="my-1">
                                                            {item.price.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}
                                                            </p>
                                                   </Col>

                                                   <Col md={2}>
                                                       <p className="my-1">
                                                        Qty: {item.qty}
                                                       </p>
                                                      
                                                   </Col>

                                                   <Col md={3}>
                                                        <p className="my-1">
                                                        Subtotal: {(item.price * item.qty).toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}
                                                       </p>
                                                   </Col>
                                               </Row>

                                           </ListGroup.Item>

                                       ))}

                                   </ListGroup>     
                                )}
                            
                            </Row>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>


                <Col md={4}>
                    <Card className="order-card my-3">
                        <ListGroup variant='flush'>
                            <ListGroup.Item>
                                <Row>
                                    <Col>Subtotal: </Col>
                                    <Col><p className="text-end">
                                        {order.itemsPrice?.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'})} 
                                        </p></Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Shipping and Handling Fee: </Col>
                                    <Col><p className="text-end">{order.shippingPrice > 0 
                                        ? order.shippingPrice?.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'}) 
                                        : 'Free'} 
                                        </p></Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col><strong>Total: </strong></Col>
                                    <Col><h4 className="text-end">{order.totalPrice?.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'})} </h4></Col>
                                </Row>
                            </ListGroup.Item>
                            
                            {!order.isPaid && (
                                <ListGroup.Item>
                                    {loadingPay && <Loader />}
                                    {!sdkReady ? (<Loader />)
                                    : (
                                        <PayPalButton amount={order.totalPrice} onSuccess={successPaymentHandler} />
                                    )}
                                </ListGroup.Item>
                            )}
                            
            
                        </ListGroup>
                    </Card>
                </Col>
            </Row>
          
        </Container>  
        </>
    )
}

export default OrderScreen
