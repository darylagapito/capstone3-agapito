/*eslint eqeqeq: "off"*/
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, Form, Button, Tab, Nav} from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { getUserProfile, updateUserProfile } from '../actions/userActions'
import { userListOrders } from '../actions/orderActions'
import { Link } from 'react-router-dom'
import UserListScreen from './UserListScreen'
import ProductListScreen from './ProductListScreen'
import OrderListScreen from './OrderListScreen'

const CMS = ({ location, history }) => {
    const [fName, setFName] = useState('')
    const [lName, setLName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState(null)

    const dispatch = useDispatch()
    const userProfile = useSelector(state => state.userProfile)
    const { loading, error, user } = userProfile

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

    const userUpdateProfile = useSelector(state => state.userUpdateProfile)
    const { success } = userUpdateProfile
    
    const orderUserList = useSelector(state => state.orderUserList)
    const { loading:loadingOrders, error:errorOrders, orders } = orderUserList
    
    useEffect(() => { 
        if(!userInfo || !userInfo.isAdmin) {
            history.push('/login')
        } else {
            if(!user.fName) {
                dispatch(getUserProfile('profile'))
                dispatch(userListOrders())
            } else {        
                setFName(user.fName)
                setLName(user.lName)
                setEmail(user.email)
                setMobileNo(user.mobileNo)
            }
        }
    }, [history, dispatch, userInfo, user])

const submitHandler = (e) => {
    e.preventDefault();   
    if (password !== confirmPassword){
        setMessage('Passwords do not match.')
    } else {
        dispatch(updateUserProfile({ 
            id: user._id,
            fName,
            lName,
            mobileNo,
            email,
            password

        }))
    }

}

const reload = () => {
    window.location.reload()
}
   
    return (

        <Tab.Container id="profile-tab" defaultActiveKey="first">
        <Row className="mt-3">
            <Col sm={3}>
            <Nav variant="pills" className="flex-column mt-3">
                <Nav.Item>
                <Nav.Link eventKey="first" onClick={reload}>Admin Profile: {userInfo.fName}</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                <Nav.Link eventKey="second">Account Details</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                <Nav.Link eventKey="third">Users</Nav.Link>
                </Nav.Item>

                <Nav.Item>
                <Nav.Link eventKey="fourth">Products</Nav.Link>
                </Nav.Item>

                <Nav.Item>
                <Nav.Link eventKey="fifth">Orders</Nav.Link>
                </Nav.Item>

            </Nav>
            </Col>

          
            <Col sm={9}>
            <Tab.Content>
                <Tab.Pane eventKey="first">
                {/* orders  */}
                <Row className="justify-content-center">
                    <Col md={9}>
                    <Row className="text-center my-3">
                    <Col>
                    <h4 >Content Management System</h4>
                    <p className="mb-5 text-danger">For Administrator Use Only</p>
                    </Col>
                </Row>
                </Col>
                </Row>
            
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                    {/* account */}
                
                <Row className="justify-content-center">
                <Col md={4}>
                <Row className="text-center my-3">
                <h4>Account Details</h4>
                <p>Manage your personal information and account details.</p>
                </Row>
                {loading && <Loader />}
                {error && <Message variant='danger'>{error}</Message>}
                {success && <Message variant='success'>Profile successfully updated.</Message>}
                {message && <Message variant='danger'>{message}</Message>}
                <Form onSubmit={ submitHandler }>
                        
                        <Form.Group controlId='fName' className="mb-3">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control 
                            required
                            type='text' 
                            placeholder="Please enter your first name" 
                            value={fName}
                            onChange={(e) => setFName(e.target.value)}>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='lName' className="mb-3">
                            <Form.Label>Last Name</Form.Label>
                            <Form.Control 
                            required
                            type='text' 
                            placeholder="Please enter your last name" 
                            value={lName}
                            onChange={(e) => setLName(e.target.value)}>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='mobileNo' className="mb-5">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control 
                            required
                            type='text' 
                            placeholder="Please enter your mobile number" 
                            value={mobileNo}
                            onChange={(e) => setMobileNo(e.target.value)}>
                            </Form.Control>
                        </Form.Group>


                        <Form.Group controlId='email' className="mb-3">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control 
                            required
                            type='email' 
                            placeholder="Please enter a valid email" 
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId='password' className="mb-3" required>
                        <Form.Label>Password</Form.Label>
                            <Form.Control 
                            required
                            type='password' 
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId='confirmPassword' className="mb-3">
                            <Form.Control 
                            required
                            type='password' 
                            placeholder="Confirm Password" 
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}>
                            </Form.Control>
                        </Form.Group>

                        <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                            UPDATE PROFILE
                        </Button>
                </Form>
                </Col>
                </Row>                    
                </Tab.Pane>
                <Tab.Pane eventKey="third">
                    <UserListScreen />
                </Tab.Pane>

                <Tab.Pane eventKey="fourth">
                    <ProductListScreen />
                </Tab.Pane>

                <Tab.Pane eventKey="fifth">
                    <OrderListScreen />
                </Tab.Pane>
            </Tab.Content>
            </Col>
        </Row>
        </Tab.Container>



     
      
      
        
   
        
        
    )
}

export default CMS
