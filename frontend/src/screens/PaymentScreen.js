import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Form, Button, Col } from 'react-bootstrap'
import Forms from '../components/Forms'
import CheckoutProcess from '../components/CheckoutProcess'
import { savePaymentMethod } from '../actions/cartActions'




const PaymentScreen = ({ history }) => {
    const cart = useSelector(state => state.cart)
    const { shippingAddress } = cart

    if(!shippingAddress) {
        history.push('/checkout')
    }

    const [paymentMethod, setPaymentMethod] = useState('Paypal')
    const [selected, setSelected] = useState('Paypal')

    const dispatch = useDispatch()

    const submitHandler = (e) => {
        e.preventDefault()
        dispatch(savePaymentMethod(paymentMethod))
        history.push('/orders')
    }
    
    return (
        <>

        <CheckoutProcess login shipping payment />
        <Forms>
            
            <Row className="text-center mt-5 mb-3">
            <h4>Payment Details</h4>
            <p>Please choose a payment method. All transactions are secure and encrypted.</p>
            </Row>

            <Form onSubmit={submitHandler} className="mt-5" required>

                <Form.Group className="mb-5">
                    <Form.Label>Payment Method:</Form.Label>
                
                <Col>
                    <Form.Check 
                    checked={selected == 'Paypal'}
                    type="radio" 
                    label="Paypal or Credit Card" 
                    id="Paypal or Credit Card"
                    name="paymentMethod"
                    value="Paypal"
                    onChange={(e) => { setPaymentMethod(e.target.value); setSelected(e.target.value) }}
                    >
                    </Form.Check>

                    <Form.Check 
                    checked={selected == 'GCash'}
                    type="radio"
                    label="GCash" 
                    id="GCash"
                    name="paymentMethod"
                    value="GCash"
                    onChange={(e) => { setPaymentMethod(e.target.value); setSelected(e.target.value) }}
                    >
                    </Form.Check>

                    <Form.Check 
                    checked={selected == 'Bank Deposit'}
                    type="radio" 
                    label="Bank Deposit/Online Transfer" 
                    id="Bank Deposit/Online Transfer"
                    name="paymentMethod"
                    value="Bank Deposit"
                    onChange={(e) => { setPaymentMethod(e.target.value); setSelected(e.target.value) }}
                    >
                    </Form.Check>

                    <Form.Check 
                    checked={selected == 'Cash on Delivery'}
                    type="radio"
                    label="Cash on Delivery" 
                    id="Cash on Delivery"
                    name="paymentMethod"
                    value="Cash on Delivery"
                    onChange={(e) => { setPaymentMethod(e.target.value); setSelected(e.target.value) }}
                    >

                    </Form.Check>
                </Col>
                </Form.Group>
                <hr />
                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                   CONTINUE TO PAYMENT
                </Button>
            </Form>
        </Forms>
      
    </>
    )
}

export default PaymentScreen
