import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Col, Form, Button } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { login } from '../actions/userActions'
import Forms from '../components/Forms'

const Login = ({ location, history }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const dispatch = useDispatch()
    const userLogin = useSelector(state => state.userLogin)
    const { loading, error, userInfo } = userLogin
    
    const redirect = location.search 
    ? location.search.split('=')[1] 
    : '/'

    useEffect(() => { 
        if(userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

const submitHandler = (e) => {
    e.preventDefault()
    dispatch(login( email, password ))

}
    return (
       <Forms>
           <Row className="text-center mt-5 mb-3">
           <h4>Log in to your account</h4>
           <p>Please enter your email and password.</p>
           </Row>
           {loading && <Loader />}
           {error && <Message variant='danger'>{error}</Message>}
           
           <Form onSubmit={ submitHandler }>
                <Form.Group controlId='email' className="mb-3">
                    <Form.Control 
                    type='email' 
                    placeholder="Email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='password' className="mb-3">
                    <Form.Control 
                    type='password' 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                    LOG IN
                </Button>
           </Form>

        
        <Row className="my-3 text-center" >
            <Col>
                <p>Don't have an account yet? 
                <Link className="mx-2 register-link" to={ redirect 
                    ? `register?redirect=${redirect}` 
                    : '/register' }>
                Sign up.
                </Link></p>
            </Col>
        </Row>

       </Forms>
    )
}

export default Login
