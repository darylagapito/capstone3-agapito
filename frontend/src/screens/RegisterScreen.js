import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Col, Form, Button } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { register } from '../actions/userActions'
import Forms from '../components/Forms'

const Register = ({ location, history }) => {
    const [fName, setFName] = useState('')
    const [lName, setLName] = useState('')
    const [mobileNo, setMobileNo] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [message, setMessage] = useState(null)

    const dispatch = useDispatch()
    const userRegister = useSelector(state => state.userRegister)
    const { loading, error, userInfo } = userRegister
    
    const redirect = location.search 
    ? location.search.split('=')[1] 
    : '/'

    useEffect(() => { 
        if(userInfo) {
            history.push(redirect)
        }
    }, [history, userInfo, redirect])

const submitHandler = (e) => {
    e.preventDefault();   

    (fName === "" && lName === "" && mobileNo === "" && email === "" && password === "") 
    ? setMessage('Please fill out all the required fields and try again.')
    : (password !== confirmPassword)
    ? setMessage('Passwords do not match.') 
    : dispatch(register(fName, lName, mobileNo, email, password ))
   

}

    return (
       <Forms>
           <Row className="text-center mt-5 mb-3">
           <h4>Create a Street Altitude account</h4>
           <p>Please fill in the information below.</p>
           </Row>
           {loading && <Loader />}
           {error && <Message variant='danger'>{error}</Message>}
           {message && <Message variant='danger'>{message}</Message>}
           <Form onSubmit={ submitHandler }>
                <Form.Group controlId='email' className="mb-3">
                    <Form.Control 
                    type='email' 
                    placeholder="Email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}>
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId='password' className="mb-3">
                    <Form.Control 
                    type='password' 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='confirmPassword' className="mb-5">
                    <Form.Control 
                    type='password' 
                    placeholder="Confirm Password" 
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='fName' className="mb-3">
                    <Form.Control 
                    type='text' 
                    placeholder="First Name" 
                    value={fName}
                    onChange={(e) => setFName(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='lName' className="mb-3">
                    <Form.Control 
                    type='text' 
                    placeholder="Last Name" 
                    value={lName}
                    onChange={(e) => setLName(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Form.Group controlId='mobileNo' className="mb-3">
                    <Form.Control 
                    type='text' 
                    placeholder="Mobile Number" 
                    value={mobileNo}
                    onChange={(e) => setMobileNo(e.target.value)}>
                    </Form.Control>
                </Form.Group>

                <Button type='submit' variant="dark" className='btn-dark col-12 p-3'>
                    SIGN UP
                </Button>
           </Form>

        
        <Row className="my-3 text-center" >
            <Col>
                <p>Already have an account? 
                <Link className="mx-2 register-link" to={ redirect 
                    ? `login?redirect=${redirect}` 
                    : '/login' }>
                Log in.
                </Link></p>
            </Col>
        </Row>

       </Forms>
    )
}

export default Register
