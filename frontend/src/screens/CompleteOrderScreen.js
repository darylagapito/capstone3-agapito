import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, ListGroup, Image, Card, Button, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { addOrder } from '../actions/orderActions'
import Message from '../components/Message'
import CheckoutProcess from '../components/CheckoutProcess'



const CompleteOrderScreen = ({ history }) => {
    const dispatch = useDispatch()
    const cart = useSelector(state => state.cart)
 
    //subtotal
    cart.itemsPrice = cart.cartItems.reduce((acc, item) => 
    acc + item.price * item.qty, 0)
    
    //shipping
    cart.shippingPrice = cart.itemsPrice > 10000 ? 0 : 350

    //total
    cart.totalPrice = cart.itemsPrice + cart.shippingPrice

const orderAdd = useSelector(state => state.orderAdd)
const { order, success, error } = orderAdd

const userLogin = useSelector(state => state.userLogin)
const { userInfo } = userLogin

useEffect(() => {
    if(!userInfo) {
        history.push('/login')
    } else {
        if(success) {
            history.push(`/orders/${order._id}`)
        }
    }
    
}, [history, success, order, userInfo])

const placeOrderHandler = () => {
    dispatch(addOrder({
        orderItems: cart.cartItems,
        shippingAddress: cart.shippingAddress,
        paymentMethod: cart.paymentMethod,
        itemsPrice: cart.itemsPrice,
        shippingPrice: cart.shippingPrice,
        totalPrice: cart.totalPrice
    }))
    
}
    

    return (
        <>
        <CheckoutProcess login shipping payment order />
        <Container>
            <Row className="text-center mt-5 mb-3">
            <h4>Order Summary</h4>
            </Row>
            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item></ListGroup.Item>
                        <ListGroup.Item>
                            <Row className="my-3">
                                <Col>
                                <span className="me-2"><strong>Ship to: </strong></span>
                                {cart.shippingAddress.address},{' '} 
                                {cart.shippingAddress.postalCode}{' '}
                                {cart.shippingAddress.city},{' '} 
                                {cart.shippingAddress.country}.
                                </Col>
                                <Col xs={1} className="text-end">
                                    <Link to="/checkout">
                                    <i className="far fa-edit text-gray"></i>
                                    </Link>
                                </Col>
                            </Row>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row className="my-3">
                                <Col>
                                <span className="me-2"><strong>Payment Method: </strong></span>
                                {cart.paymentMethod}
                                </Col>
                                <Col xs={1} className="text-end">
                                    <Link to="/payment">
                                    <i className="far fa-edit text-gray"></i>
                                    </Link>
                                </Col>
                            </Row>
                            
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <Row className="my-3">
                                <span className="me-2"><strong>Items: </strong></span>
                                {cart.cartItems.length === 0 ? <Message>You cart is empty</Message>
                                : (
                                   <ListGroup variant='flush'>
                                       {cart.cartItems.map((item, index) => (
                                           <ListGroup.Item key={index} >
                                               <Row className="my-3">
                                                   <Col md={2}>
                                                       <Image src={item.image} alt={item.name} fluid />
                                                   </Col>

                                                   <Col>
                                                        <Link to={`/product/${item.product}`}>
                                                            <p className="my-1">
                                                            {item.name}
                                                            </p>
                                                        </Link>
                                                            <p className="my-1">
                                                            {item.price.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}
                                                            </p>
                                                   </Col>

                                                   <Col md={2}>
                                                       <p className="my-1">
                                                        Qty: {item.qty}
                                                       </p>
                                                      
                                                   </Col>

                                                   <Col md={3}>
                                                        <p className="my-1">
                                                        Subtotal: {(item.price * item.qty).toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}
                                                       </p>
                                                   </Col>
                                               </Row>

                                           </ListGroup.Item>

                                       ))}

                                   </ListGroup>     
                                )}
                            
                            </Row>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>


                <Col md={4}>
                    <Card className="order-card my-3">
                        <ListGroup variant='flush'>
                            <ListGroup.Item>
                                <Row>
                                    <Col>Subtotal: </Col>
                                    <Col><p className="text-end">
                                        {cart.itemsPrice.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'})} 
                                        </p></Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col>Shipping and Handling Fee: </Col>
                                    <Col><p className="text-end">{cart.shippingPrice > 0 
                                        ? cart.shippingPrice.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'}) 
                                        : 'Free'} 
                                        </p></Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                <Row>
                                    <Col><strong>Total:</strong></Col>
                                    <Col><h4 className="text-end">{cart.totalPrice.toLocaleString('en-PH', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'PHP'})} </h4></Col>
                                </Row>
                            </ListGroup.Item>

                            <ListGroup.Item>
                                {error && <Message variant="danger">{error}</Message>}
                                <Button 
                                type='submit' 
                                variant="dark" 
                                className='btn-dark col-12 p-3'
                                disabled={userInfo.isAdmin}
                                onClick={placeOrderHandler}>
                                COMPLETE ORDER
                                </Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </Col>
            </Row>
          
        </Container>  
        </>
    )
}

export default CompleteOrderScreen
