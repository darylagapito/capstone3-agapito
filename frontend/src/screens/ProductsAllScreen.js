import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { listProducts } from '../actions/productActions'
import { Row, Col, Carousel, Button  } from 'react-bootstrap'
import Product from '../components/Product'
import Loader from '../components/Loader'
import Message from '../components/Message'


const ProductAllScreen = () => {
   const dispatch = useDispatch()
   const productList = useSelector(state => state.productList)
   const { loading, error, products } = productList 

    useEffect(() => {
        dispatch(listProducts())
    }, [dispatch])
       

    return (
        <>
           <Carousel fade >
            <Carousel.Item>
                <img
                className=" w-100"
                src="/images/p1.jpg"
                alt="First slide"
                />
                <Carousel.Caption className="caption">
                    <h1>Get Your Hands on the Latest Sneakers</h1>
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/p2.jpg"
                alt="Second slide"
                />

                <Carousel.Caption className="caption">
                <h1>Find the Best Brands and Deals</h1>

                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/p3.jpg"
                alt="Third slide"
                />

                <Carousel.Caption className="caption">
                <h1>Guaranteed Authentic and Rare Sneakers</h1>
           
                </Carousel.Caption>
            </Carousel.Item>

        
            </Carousel>

        <Row className="my-5 text-center">
        <h4>Our Sneakers Collection</h4> 
        </Row>
             
            {loading ? (<Loader />) : error 
            ? (<Message variant='danger'>{error}</Message>) 
            : ( 
            <Row>
                {products.map((product,index) => (
                    product.isActive ? 
                    <Col sm={12} md={4} lg={3} key={index} className="my-3">
                        <Product product={product} />
                    </Col>
                    : null
                ))}
            </Row>
            )}
            
        </>
    )
}

export default ProductAllScreen
