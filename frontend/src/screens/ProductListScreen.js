import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Row, Col, Button, Table, Accordion, Card } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { listProducts, deleteProduct, createProduct } from '../actions/productActions'
import { PRODUCT_CREATE_RESET } from '../constants/productConstants'
import { Link } from 'react-router-dom'

const ProductListScreen = ({ match, history, location }) => {
    const dispatch = useDispatch()

    const productList = useSelector(state => state.productList)
    const { loading, error, products } = productList

    const productDelete = useSelector(state => state.productDelete)
    const { loading:loadingDelete, error:errorDelete, success } = productDelete

    const productCreate = useSelector(state => state.productCreate)
    const { loading:loadingCreate, 
        error:errorCreate, 
        success:successCreate, 
        product:createdProduct 
    } = productCreate

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin

  
    useEffect(() => {
        dispatch({ type: PRODUCT_CREATE_RESET })
        if(!userInfo.isAdmin) {
            history.push('/login')
        } 

        if(successCreate) {
            window.location.replace(`/admin/products/${createdProduct._id}/edit`)
        }else{
            dispatch(listProducts())
        }

 
    }, [dispatch, history, userInfo, success, successCreate, createdProduct])

    const deleteHandler = (id) => {
        if (window.confirm('Are you sure you want to delete this user?')){
            dispatch(deleteProduct(id))
            window.location.replace('/admin/CMS')
        }
        window.location.replace('/admin/CMS')
    }

    const createProductHandler = () => {
        dispatch(createProduct())
    }

    return (
        <>
      
        <Row className="justify-content-center">
        <Col md={9}>
        <Row className="text-center my-3">
        <Col>
        <h4 >Products</h4>
        <p className="mb-5 text-danger">For Administrator Use Only</p>

        <Row className="text-end">
            <Col>
                <Button variant="dark" className="border-0 mb-1" onClick={createProductHandler}><i className="fas fa-plus"></i></Button>
            </Col>
        </Row>
        
        {loadingDelete && <Loader />}
        {errorDelete && <Message variant='danger'>{errorDelete}</Message> }
        {loadingCreate && <Loader />}
        {errorCreate && <Message variant='danger'>{errorCreate}</Message> }
        
        {loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message>
        : ( 
        
        <>
            <Accordion id="user-list-accordion" className="text-start" defaultActiveKey="0">
                {products.map(product => (
                    <Card key={product._id} id="user-card" className="mb-1" key={product._id}>
                    
                        <Accordion.Toggle as={Card.Header}  eventKey={product._id}>
                            <Col>
                            product ID: {product._id} 
                            </Col>
                            <Col>
                            <strong>{product.name}</strong>
                            </Col>
                        </Accordion.Toggle>
                        <Accordion.Collapse className="accordion-collapse" eventKey={product._id}>
                        <Card.Body>
                            <Table borderless hover responsive="xs" className="text-center">
                                <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>Brand</th>
                                        <th>Price</th>
                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                            <td className="col-9 text-start">{product.description}</td>
                                            <td>{product.brand}</td>
                                           
                                            
                                            <td>{product.price.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'})}</td>

                                    </tr>
                                </tbody>

                                   
                            </Table>
                            
                            <Row>
                                <Col>
                                <p className="my-3"><strong>Sizes Available:</strong></p>
                                <Table bordered className="text-center my-3">
                                <thead>
                                    <tr>
                                        {
                                            product.sizeInStock.map((size, index) => (
                                                Object.values(size) > 0 ?
                                                <th key={index}>{Object.keys(size)}</th> : null
                                        ))}
                                      
                                    </tr>
                                </thead>
                                <tbody className="border-1">
                                    <tr>
                                            {
                                            product.sizeInStock.map((size, i) => (
                                                Object.values(size) > 0 ?
                                                <td key={i} className="p-1 border-1">{Object.values(size)}</td> : null
                                            ))}
                                    </tr>
                                </tbody>
                            </Table>
                                
                                </Col>
                            </Row>


                            <Row>   
                            <Col>
                            

                           
                            <LinkContainer to={`/admin/products/${product._id}/edit`}>
                                <Button variant='light' className='border-0 btn-sm user-list-buttons'>
                                    <i className="fas fa-edit"></i>
                                </Button>
                            </LinkContainer>
                            </Col>

                            <Col className="text-end">
                            <Button variant='light' className='border-0 user-list-buttons btn-sm border-0' onClick={() => deleteHandler(product._id)}><i className="fas fa-trash text-danger"></i></Button> 
                            </Col>
                            </Row>

                            
                           
                            
    
                        </Card.Body>
                        </Accordion.Collapse>

                    </Card>
                 ))}
                </Accordion>
                                         
                </>  
               
            )}
        
        </Col>
    </Row>
    </Col>
    </Row>

    </>
    )
}

export default ProductListScreen
