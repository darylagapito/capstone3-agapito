import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, Image, ListGroup, Button } from 'react-bootstrap'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { listProductDetails } from '../actions/productActions'


const ProductScreen = ({ history, match }) => {
    
    const [size, setSize] = useState()
    const [qty] = useState(1)

    const dispatch = useDispatch()
    const productDetails = useSelector(state => state.productDetails)
    const { loading, error, product } = productDetails

    //prod gallery
    const image = product.imageGallery
    const [selectedImg, setSelectedImg] = useState()

    useEffect(() => {
        dispatch(listProductDetails(match.params.id))
      
    }, [dispatch, match])

    const addToCartHandler = () => {
        history.push(`/cart/${match.params.id}?s=${size}?q=${qty}`)
    }
    console.log(product.sizeInStock)
    return (
        <>
        
            <Link to={`/products`}><p className="mt-4 return"><span className="me-2">&#10094;</span> Back</p></Link>

            { loading ? <Loader /> : error ? <Message variant='danger'>{error}</Message> :
            
            <Row>
                { product.image && 
                
                <Col md={7} className="mt-5 justify-content-center">
                    <Image id="big" src={selectedImg || image[1]} alt="Selected" fluid />
                
                    <Row className="text-center justify-content-center">
                        {image.map((img, index) => (
                        <Col xs={2}  className="my-3" key={index}>
                        <Image className="thumbnail" src={img} onClick={() => setSelectedImg(img)} fluid />
                        </Col>
                        ))
                        }
                    </Row>
                </Col> 
                } 
                
                <Col md={{span: 4, offset: 1 }} >
                    <ListGroup variant="flush">
                        <ListGroup.Item  className="no-border mt-5">
                            <h2><strong>{product.name}</strong></h2>
                        </ListGroup.Item>
                        
                        <ListGroup.Item  className="mb-3 no-border">
                            <p>
                                { product.brand }
                            </p>
                            <p>
                                Price: { product.price?.toLocaleString('en-PH', {maximumFractionDigits: 0, style: 'currency', currency: 'PHP'}) }
                             
                            </p>
                        </ListGroup.Item>

                        <ListGroup.Item className="no-border">
                            <p>
                                { product.description }
                            </p>
                        </ListGroup.Item>
                            <ListGroup.Item className="no-border">
                                <Row className="mt-3">
                                    <Col md={6}>
                                        Men's US Size:
                                    </Col>
                                </Row>

                                
                                <Row className="justify-content-center">
                                    <Col>
                                        <ul className="size-swatch">
                                        
                                        {//Primary IF
                                       
                                        product.sizeInStock && 
                                           product.sizeInStock.map((x, index) => (
                                               Object.values(x) > 0 
                                               ? 
                                            <li key={index} >
                                            {/* eslint-disable-next-line */}
                                            <label className={size == Object.keys(x) ? 'checked' : null } htmlFor={Object.keys(x)}>{Object.keys(x)}</label>
                                            <input type="radio"
                                                    id={Object.keys(x)}
                                                    checked={size === Object.keys(x)}
                                                    value={Object.keys(x)}
                                                    onChange={(e) => { setSize(e.target.value) }}
                                                    
                                            />
                                            </li>

                                            : null //hide not in stock
                                            ))
                                    
                                        }    
                                        </ul>


                                    </Col>
                                </Row> 

                            </ListGroup.Item>
                          

                            <ListGroup.Item className="no-border">
                                
                                    {product.sizeInStock && product.sizeInStock.length === 0 ? 
                                    <Button
                                    className='btn-light col-12 p-3 border'
                                    type='button'
                                    disabled
                                    >OUT OF STOCK </Button>
                                    : 
                                    <Button
                                    onClick={size && addToCartHandler}
                                    className='btn-dark col-12 p-3'
                                    type='button'
                                    >ADD TO CART</Button>
                                    }
                                    
                            </ListGroup.Item>

                    </ListGroup>
                </Col>
            </Row>

            }


        </>
            
    )
    
}


export default ProductScreen
