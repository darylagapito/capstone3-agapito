import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { listProducts } from '../actions/productActions'
import { Row, Col, Carousel, Card, Button, Image, Container  } from 'react-bootstrap'
import Product from '../components/Product'
import Loader from '../components/Loader'
import Message from '../components/Message'
import { LinkContainer } from 'react-router-bootstrap'


const HomeScreen = () => {
   const dispatch = useDispatch()
   const productList = useSelector(state => state.productList)
   const { loading, error, products } = productList 

    useEffect(() => {
        dispatch(listProducts())
    }, [dispatch])
       

    return (
        <>

        
            <Carousel fade className="pb-5" >
            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/1.jpg"
                alt="First slide"
                />
                <Carousel.Caption className="caption text-end">
                    <h1>Get Your Hands on the Latest Sneakers</h1>
                    <p>Be the first to get the new Air Jordan 1s, Yeezys, and other awesome pairs.</p>
                    <Button type='link' href="/product/60f8b02227886475c0f08c69" className='c-button btn-dark col-12 col-md-3 p-3 mt-2 mb-5'>
                            GET THE LATEST PAIR
                    </Button> 
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/2.jpg"
                alt="Second slide"
                />

                <Carousel.Caption className="caption">
                <h1>Find the Best Brands and Deals</h1>
                <p>Shop for the lifestyle that you want.</p>
                <Button type='link' href="/product/60f8b02227886475c0f08c69" className='c-button btn-dark col-3 p-3 mt-2 mb-5'>
                            CHECK OUR COLLECTION
                    </Button> 
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/3.jpg"
                alt="Third slide"
                />

                <Carousel.Caption className="caption text-start">
                <h1>Explore the Latest Trends Among Sneaker Heads </h1>
                <p>Check out our blog for the latest news and hot topics.</p>
                <Button type='link' href="/product/60f8b02227886475c0f08c69"  className='c-button btn-dark col-3 p-3 mt-2 mb-5'>
                            VISIT OUR BLOG
                    </Button> 
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src="/images/4.jpg"
                alt="Third slide"
                />

                <Carousel.Caption className="caption text-start">
                
                <h1>Jordan 1 Retro Collection</h1>
                <p>Discover the rarest pairs of retro sneakers in the market.</p>
                <Button type='link' href="/products" variant="dark" className='c-button btn-dark col-3 p-3 mt-2 mb-5'>
                           EXPLORE SNEAKERS
                    </Button> 
                </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item >
                <img
                className="d-block w-100"
                src="/images/5.jpg"
                alt="Third slide"
                />

                <Carousel.Caption className="caption">
                <h1>Nike Air Force 1 Collection</h1>
                <p>Find the Best Collection of AF1's in the Country.</p>
                <Button type='link' href="/products" variant="dark" className='c-button btn-dark col-3 p-3 mt-2 mb-5'>
                           SHOP AIR FORCE 1
                    </Button> 
                </Carousel.Caption>
            </Carousel.Item>
            </Carousel>



        <Container className="my-5">
        <Row className="my-5 partition">
            <h4 className="text-center">Featured Sneakers</h4>  
        </Row>
          
            {loading ? (<Loader />) : error 
            ? (<Message variant='danger'>{error}</Message>) 
            : ( 
            <Row>
                {products.slice(0, 4).map((product,index) => (
                    product.isActive ? 
                    <Col sm={12} md={3} lg={3} key={index} className="my-2">
                        <Product product={product} />
                    </Col>
                    : null
                ))}
            </Row>
            )}


            <hr className="mt-5" />
            <Row className="my-5 justify-content-center partition">
            <h4 className="my-5 text-center">Shop By Collection</h4>  
           
                <Col className="4 mb-5">
                    <LinkContainer to="/products">
                    <Image className="border-0 category-card" src="/images/b1.jpg" fluid />
                    </LinkContainer>
                </Col>

                <Col className="4 mb-5">
                    <LinkContainer to="/products">
                    <Image className="border-0 category-card" src="/images/b2.jpg" fluid />
                    </LinkContainer>
                </Col>

                <Col className="4 mb-5">
                    <LinkContainer to="/products">
                    <Image className="border-0 category-card" src="/images/b3.jpg" fluid />
                    </LinkContainer>
                </Col>
            </Row>



            
                    <Row className="justify-content-center text-center my-5">
                        <Col>
                        <LinkContainer to="/products">
                            <Image src="/images/banner.jpg" className="mx-auto mb-5 category-card" fluid/>
                        </LinkContainer>    
                        </Col>
                    </Row>

                <hr className="my-5"/>
                <Row className="my-5 partition">
                    <h4 className="text-center">Hype Collection</h4>  
                </Row>

                    {loading ? (<Loader />) : error 
            ? (<Message variant='danger'>{error}</Message>) 
            : ( 
            <Row>
                {products.slice(4, 8).map((product,index) => (
                    product.isActive ? 
                    <Col sm={12} md={3} lg={3} key={index} className="my-2">
                        <Product product={product} />
                    </Col>
                    : null
                ))}
            </Row>
            )}

            </Container>
        </>
    )
}

export default HomeScreen
