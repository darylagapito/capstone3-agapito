import mongoose from 'mongoose'

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },

    image: {
        type: String,
        required: true
    },

    imageGallery: [],

    description: {
        type: String,
        required: true
    },

    brand: {
        type: String
    },

    category: {
        type: String
    },

    price: {
        type: Number,
        required: true
    },

    sizeInStock: [mongoose.Schema.Types.Mixed],

    isActive: {
        type: Boolean,
        default: false
    }

},{
    timestamps: true
})

const Product = mongoose.model('Product', productSchema)

export default Product