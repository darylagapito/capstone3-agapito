import asyncHandler from 'express-async-handler'
import User from '../models/userModel.js'
import generateToken from '../auth.js'

//Registration
const registerUser = asyncHandler( async(req, res) => {
    const { fName, lName, mobileNo, email, password } = req.body
      
    const userExists = await User.findOne({ email })

    if(userExists){
        res.status(400)
        throw new Error('The email address you provided is already associated with an account.')
    }

    const user = await User.create({
        fName,
        lName,
        mobileNo,
        email,
        password
    })

    if(user) {
        res.status(201).json({
            _id: user._id,
            fName: user.fName,
            lName: user.lName,
            mobileNo: user.mobileNo,
            isAdmin: user.isAdmin,
            token: generateToken(user._id)
        })
    } else {
        res.status(400)
        throw new Error('Invalid information provided. Please try again.') 
    }
})



//login - authenticate
const authUser = asyncHandler( async(req, res) => {
  const { email, password } = req.body
    
  const user = await User.findOne({ email })
    if(user && (await user.matchPassword(password))) {
        res.json({
            _id: user._id,
            fName: user.fName,
            lName: user.lName,
            email: user.email,
            isAdmin: user.isAdmin,
            token: generateToken(user._id)

        })
    } else {
        res.status(401)  
        throw new Error('Incorrect email or password.')
    }
})


//View Profile
const getUserProfile = asyncHandler(async (req, res) => {
    const user = req.user
    
    if(user) {
        
        res.json({
            _id: user._id,
            fName: user.fName,
            lName: user.lName,
            mobileNo: user.mobileNo,
            email: user.email,
        })
    } else {
        res.status(404)
        throw new Error('User not found.')
    }
})

//Update Profile
const updateUserProfile = asyncHandler(async (req, res) => {
    const user = req.user
    if(user) {
        user.fName = req.body.fName || user.fName
        user.lName = req.body.lName || user.lName
        user.mobileNo = req.body.mobileNo || user.mobileNo
        user.email = req.body.email || user.email
        if(req.body.password) {
            user.password = req.body.password
        }

        const updatedUser = await user.save()
        res.json({
            _id: updatedUser._id,
            fName: updatedUser.fName,
            lName: updatedUser.lName,
            mobileNo: updatedUser.mobileNo,
            email: updatedUser.email,
        })   
        
       
    } else {
        res.status(404)
        throw new Error('User not found.')
    }
})


const getUsers = asyncHandler(async (req, res) => {
    const users = await User.find({})
    res.json(users)
})

const deleteUser = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id)
    
    if(user) {
        await user.remove()
        res.json({ message: 'User successfully deleted.'})
    } else {
        res.status(404)
        throw new Error('User not found.')
    }
})

const getUserByAdmin = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id).select('-password')
    
    if(user){
        res.json(user)
    } else {
        res.status(404)
        throw new Error('User not found.')
    }
})

const updateUserByAdmin = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id)
    
    if(user) {
        user.fName = req.body.fName || user.fName
        user.lName = req.body.lName || user.lName
        user.mobileNo = req.body.mobileNo || user.mobileNo
        user.email = req.body.email || user.email
        user.isAdmin = req.body.isAdmin
        const updatedUser = await user.save()
        
        res.json({
            _id: updatedUser._id,
            fName: updatedUser.fName,
            lName: updatedUser.lName,
            mobileNo: updatedUser.mobileNo,
            email: updatedUser.email,
            isAdmin: updatedUser.isAdmin
        })   
        
       
    } else {
        res.status(404)
        throw new Error('User not found.')
    }
})


export {
    authUser,
    registerUser,
    getUserProfile,
    updateUserProfile,
    getUsers,
    deleteUser,
    getUserByAdmin,
    updateUserByAdmin
}

