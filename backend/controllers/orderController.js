import asyncHandler from 'express-async-handler'
import Order from '../models/orderModel.js'


const addOrder = asyncHandler( async(req, res) => {
   const { 
    orderItems, 
    shippingAddress, 
    paymentMethod, 
    itemsPrice, 
    shippingPrice, 
    totalPrice 
    } = req.body
    
    
    if(orderItems && orderItems.length === 0) {
        res.status(400)
        throw new Error('No orders in shopping cart.')
        return
    } else {
        const order = new Order({
        orderItems,
        user: req.user._id, 
        shippingAddress,
        paymentMethod,
        itemsPrice,
        shippingPrice,
        totalPrice
        })
        const createdOrder = await order.save()
        res.status(201).json(createdOrder)
    }
})

const getOrder = asyncHandler( async(req, res) => {
    const order = await Order.findById(req.params.id).populate('user', 'fName lName email mobileNo')

    if(order) {
        res.json(order)
    } else {
        res.status(404)
        throw new Error('Order not found.')
    }
 })


 const payOrder = asyncHandler( async(req, res) => {
    const order = await Order.findById(req.params.id)
   
    if(order) {
        order.isPaid = true
        order.paidAt = Date.now()
        order.paymentResult = {
            id: req.body.id,
            status: req.body.status,
            update_time: req.body.update_time,
            email_address: req.body.payer.email_address
        }

        const updatedOrder = await order.save()

        res.json(updatedOrder)
    } else {
        res.status(404)
        throw new Error('Order not found.')
    }
 })


 const getUserOrders = asyncHandler( async(req, res) => {
    const orders = await Order.find({ user: req.user.id })
    res.json(orders)
 
 })

 const getAllOrders = asyncHandler( async(req, res) => {
     console.log('fired')
    const orders = await Order.find({}).populate('user', 'id fName lName')
    res.json(orders)
 })

export {
    addOrder,
    getOrder,
    payOrder,
    getUserOrders,
    getAllOrders
}