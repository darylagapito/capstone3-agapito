import asyncHandler from 'express-async-handler'
import Product from '../models/productModel.js'


const getProducts = asyncHandler( async(req, res) => {
    const products = await Product.find({})
    res.json(products)
})

const getProductId = asyncHandler( async(req, res) => {
    const product = await Product.findById(req.params.id)
    
    if(product) {
        res.json(product)     
    } else {
        res.status(404)
        throw new Error('Item not found.')
    }
}) 

const deleteProduct = asyncHandler( async(req, res) => {
    const product = await Product.findById(req.params.id)
    
    if(product) {
        await product.remove()
        res.json({ message: 'Product successfully deleted.'})
    } else {
        res.status(404)
        throw new Error('Item not found.')
    }
}) 


//ADD
const addProduct = asyncHandler( async(req, res) => {
    const product = await Product.create({
        name: 'New Product', 
        description: 'Description', 
        brand: 'Brand', 
        category: 'Category', 
        price: 0, 
        image: 'Enter a main image url',
        sizeInStock: [{"8․5": 0},{"9": 0}, {"9․5": 0}, {"10": 0}, {"10․5": 0}, {"11": 0}, {"11․5": 0}, {"12": 0}]
    })

    const createdProduct = await product.save({ checkKeys: false })
    res.status(201).json(createdProduct)
})

const updateProduct = asyncHandler(async (req, res) => {
    const { name, description, brand, category, price, image, sizeInStock, isActive } = req.body

    const product = await Product.findById(req.params.id)
    
    if(product) {
        
        product.name = name || product.name
        product.description = description || product.description, 
        product.brand = brand || product.brand, 
        product.category = category || product.category, 
        product.price = price || product.price, 
        product.image = image || product.image, 
        product.sizeInStock = sizeInStock || product.sizeInStock
        product.isActive = isActive 

        const updatedProduct = await product.save()
        res.json(updatedProduct)   
        
    } else {
        res.status(404)
        throw new Error('Product not found.')
    }
})


export {
    getProducts,
    getProductId,
    deleteProduct,
    addProduct,
    updateProduct
}