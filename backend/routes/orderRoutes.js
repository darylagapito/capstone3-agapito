import express from 'express'
const router = express.Router()
import { addOrder, getOrder, payOrder, getUserOrders, getAllOrders } from '../controllers/orderController.js'
import { adminAuth, authenticate } from '../auth.js'

router.route('/').post(authenticate, addOrder).get(authenticate, adminAuth, getAllOrders)
router.route('/userorders').get(authenticate, getUserOrders)
router.route('/:id').get(authenticate, getOrder)
router.route('/:id/pay').put(authenticate, payOrder)


export default router 