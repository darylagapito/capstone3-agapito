import express from 'express'
const router = express.Router()
import { authUser, registerUser, getUserProfile, updateUserProfile, getUsers, deleteUser, getUserByAdmin, updateUserByAdmin } from '../controllers/userController.js'
import { adminAuth, authenticate } from '../auth.js'

router.route('/').post(registerUser).get(authenticate, adminAuth, getUsers)
router.route('/login').post(authUser)
router.route('/profile').get(authenticate, getUserProfile).put(authenticate, updateUserProfile)
router.route('/:id').delete(authenticate, adminAuth, deleteUser)
                    .get(authenticate, adminAuth, getUserByAdmin)
                    .put(authenticate, adminAuth, updateUserByAdmin)

export default router