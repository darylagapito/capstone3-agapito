import express from 'express'
import { adminAuth, authenticate } from '../auth.js'
const router = express.Router()
import { getProducts, getProductId, deleteProduct, addProduct, updateProduct } from '../controllers/productController.js'
import { authUser } from '../controllers/userController.js'


router.route('/').get(getProducts)
router.route('/').post(authenticate, adminAuth, addProduct)
router.route('/:id').get(getProductId)
                    .delete(authenticate, adminAuth, deleteProduct)
                    .put(authenticate, adminAuth, updateProduct)   

export default router